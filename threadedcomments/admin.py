from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.comments.admin import CommentsAdmin
from django.contrib.contenttypes.models import ContentType

from threadedcomments.models import ThreadedComment

def get_related_display(obj):

    ct = ContentType.objects.get(name=obj.content_type)
    return ct.get_object_for_this_type(id=obj.object_pk).__unicode__()
get_related_display.short_description = "Related Content"

class ThreadedCommentsAdmin(CommentsAdmin):
    fieldsets = (
        (None,
           {'fields': ('content_type', 'object_pk', 'site')}
        ),
        (_('Content'),
           {'fields': ('user', 'user_name', 'user_email', 'user_url', 'comment')}
        ),
        (_('Hierarchy'),
           {'fields': ('parent',)}
        ),
        (_('Metadata'),
           {'fields': ('submit_date', 'ip_address', 'is_public', 'is_removed', 'spam_flag')}
        ),
    )

    list_display = ('name', get_related_display,
                    'comment', 'submit_date', 'is_public', 'is_removed', 'spam_flag')
    search_fields = ('comment', 'user__username', 'user_name',
                     'user_email', 'user_url', 'ip_address')
    raw_id_fields = ("parent",)

    list_filter = ('is_public', 'spam_flag', 'submit_date', 'user__username')

    def mark_as_spam(self, request, queryset):
        rows_updated = queryset.update(spam_flag=True)
        if rows_updated == 1:
            msg = "1 comment was"
        else:
            msg = "%s stories were" % rows_updated
        self.message_user(request, "%s marked as spam." % msg)
    mark_as_spam.short_description = "Mark comments as spam"

    def mark_as_not_spam(self, request, queryset):
        rows_updated = queryset.update(spam_flag=False)
        if rows_updated == 1:
            msg = "1 comment was"
        else:
            msg = "%s stories were" % rows_updated
        self.message_user(request, "%s marked as not spam." % msg)
    mark_as_not_spam.short_description = "Mark comments as NOT spam"

    actions = [ mark_as_spam, mark_as_not_spam ]

admin.site.register(ThreadedComment, ThreadedCommentsAdmin)

